#ifndef PHYSICS_PARABOLIC_MOVEMENT_HPP
#define PHYSICS_PARABOLIC_MOVEMENT_HPP 

#include <cmath>
#include <vector>
#include <fstream>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <Tools.hpp>

namespace physics {
  struct ParabolicData {
    double t;
    double x;
    double y;
    double sx;
    double sy;
  };
}

namespace physics::ideal {

  double parabolic_x(double instant, double initial_speed, double initial_angle) {
    return initial_speed * std::cos(convert::deg_to_rad(initial_angle)) * instant;
  }

  double parabolic_y(double instant, double initial_speed, double initial_angle, double gravity) {
    return initial_speed * std::sin(convert::deg_to_rad(initial_angle)) * instant - 0.5*gravity*instant*instant;
  }

  double analytic_speed_x(double initial_speed, double initial_angle) {
    return initial_speed * std::cos(convert::deg_to_rad(initial_angle));
  }

  double analytic_speed_y(double initial_speed, double initial_angle, double gravity, double instant) {
    return initial_speed * std::sin(convert::deg_to_rad(initial_angle)) - gravity * instant;
  }

  double numeric_speed_y(double speed_k, double gravity, double time_step) {
    return speed_k - gravity * time_step;
  }

  std::vector<ParabolicData> parabolic_movement_ideal(double initial_speed, double initial_angle, double gravity = 9.8, double step = 0.01) {
    ParabolicData              data;
    std::vector<ParabolicData> all_data;

    data.sx = initial_speed * std::cos(convert::deg_to_rad(initial_angle));
    data.sy = initial_speed * std::sin(convert::deg_to_rad(initial_angle));
    data.x  = 0.0;
    data.y  = 0.0;
    data.t  = 0.0;

    all_data.push_back(data);

    do {

      data.x  = parabolic_x(data.t, initial_speed, initial_angle);
      data.y  = parabolic_y(data.t, initial_speed, initial_angle, gravity);
      data.sx = analytic_speed_x(initial_speed, initial_angle);
      data.sy = analytic_speed_y(initial_speed, initial_angle, gravity, data.t);

      all_data.push_back(data);

      data.t += step;
    } while (comp::ge(data.y, 0.0));

    return std::move(all_data);
  }

}

namespace physics::real {
  double position_numeric(double curr_p, double curr_speed, double time_delta) {
    return curr_p + curr_speed * time_delta;
  }

  double speed_numeric(double curr_speed, double curr_acceleration, double time_delta) {
    return curr_speed + curr_acceleration * time_delta;
  }

  double acceleration_x(double c, double ro, double area, double mass, double speed_x, double speed) {
    static double k = 0.5 * c * area * ro / mass;
    return -k * speed_x * speed;
  }

  double acceleration_y(double c, double ro, double area, double mass, double speed_y, double speed, double gravity) {
    static double k = 0.5 * c * area * ro / mass;
    return -gravity - k * speed_y * speed;
  }


  double speed_from_components(double sx, double sy) {
    return std::sqrt(sx*sx + sy*sy);
  }

  std::vector<ParabolicData> parabolic_movement_real(double mass, double initial_speed, double initial_angle, double c, double ro, 
                                                     double area, double gravity = 9.8, double step = 0.01) {
    ParabolicData              data;
    std::vector<ParabolicData> all_data;

    data.sx = initial_speed * std::cos(convert::deg_to_rad(initial_angle));
    data.sy = initial_speed * std::sin(convert::deg_to_rad(initial_angle));
    data.x  = 0.0;
    data.y  = 0.0;
    data.t  = 0.0;

    all_data.push_back(data);

    double &speed = initial_speed;
    double a_x = 0.0;
    double a_y = 0.0;

    do {
      speed  = speed_from_components(data.sx, data.sy);
      data.x = position_numeric(data.x, data.sx, step);
      data.y = position_numeric(data.y, data.sy, step);
      a_x = acceleration_x(c, ro, area, mass, data.sx, speed);
      a_y = acceleration_y(c, ro, area, mass, data.sy, speed, gravity);
      data.sx = speed_numeric(data.sx, a_x, step);
      data.sy = speed_numeric(data.sy, a_y, step);

      all_data.push_back(data);

      data.t += step;
    } while (comp::ge(data.y, 0));

    return all_data;
  }
}

#endif

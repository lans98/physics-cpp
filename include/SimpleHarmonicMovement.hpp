#pragma once

#include <Tools.hpp>

#include <vector>

namespace physics {
  struct PendulumData {
    double time;
    double angle;
    double speed;
  };

  struct SpringData {
    double time;
    double speed;
    double position;
  };
}

namespace physics::ideal {

  std::vector<PendulumData> pendulum(double initial_speed, double initial_angle, double cable_length,
                                     double gravity = constants::earth_gravity, unsigned long steps = 1000, double step = 0.001)  {
    PendulumData              data;
    std::vector<PendulumData> all_data;

    data.time  = 0.0;
    data.angle = initial_angle;
    data.speed = 0.0;

    double acceleration = 0.0;

    for (unsigned long ii = 0; ii < steps; ++ii) {

      all_data.push_back(data);

      acceleration = -gravity * data.angle / cable_length;
      data.angle = methods::euler(data.angle, data.speed, step);
      data.speed = methods::euler(data.speed, acceleration, step);

      data.time += step;
    }

    return all_data;
  }

  std::vector<SpringData> spring(double mass, double k, double initial_pos, double initial_speed, unsigned long steps, double step = 0.001) {
    SpringData              data;
    std::vector<SpringData> all_data;

    data.time  = 0.0;
    data.speed = initial_speed;
    data.position = initial_pos;

    double acceleration = 0.0;

    for (unsigned long ii = 0; ii < steps; ++ii) {
      all_data.push_back(data);

      acceleration  = (-k * data.position) / mass;
      data.position = methods::euler(data.position, data.speed, step);
      data.speed    = methods::euler(data.speed, acceleration, step);

      data.time += step;
    }

    return all_data;
  }

}

#ifndef PHYSICS_TOOLS_HPP
#define PHYSICS_TOOLS_HPP

#include <cmath>

namespace physics::constants {
  const double pi = 3.141592653589793;
  const double e  = 2.718281828459045;
  const double earth_gravity = 9.80665;
}

namespace physics::methods {
  double euler(double a, double b, double step) {
    return a + b * step;
  }
}

namespace physics::convert {
  double deg_to_rad(double deg) { return deg * constants::pi / 180.0; }
}

namespace physics::calc {
  double circle_area(double radio) { return constants::pi * radio * radio; }
}

namespace physics::comp {
  bool eq(double a, double b, int accuracy = 4) {
    // Check integral parts first
    long int_a = static_cast<long>(a);
    long int_b = static_cast<long>(b);

    // If integral parts aren't equal, return false
    if (int_a != int_b) return false;

    // If they are equal, we can now check if it's decimal parts are equal
    long dec_a = (a - int_a) * (std::pow(10, accuracy));
    long dec_b = (b - int_b) * (std::pow(10, accuracy));
    return dec_a == dec_b;
  }

  bool lt(double a, double b, int accuracy = 4) {
    // Check integral parts first
    long int_a = static_cast<long>(a);
    long int_b = static_cast<long>(b);

    // If a isn't less or equal b, return false
    if (int_a > int_b) return false;
    if (int_a < int_b) return true;

    long dec_a = (a - int_a) * (std::pow(10, accuracy));
    long dec_b = (b - int_b) * (std::pow(10, accuracy));
    return dec_a < dec_b;
  }

  bool gt(double a, double b, int accuracy = 4) {
    // Check integral parts first
    long int_a = static_cast<long>(a);
    long int_b = static_cast<long>(b);

    // If a isn't greater or equal b, return false
    if (int_a < int_b) return false;
    if (int_a > int_b) return true;

    long dec_a = (a - int_a) * (std::pow(10, accuracy));
    long dec_b = (b - int_b) * (std::pow(10, accuracy));
    return dec_a > dec_b;
  }

  bool le(double a, double b, int accuracy = 4) {
    // Check integral parts first
    long int_a = static_cast<long>(a);
    long int_b = static_cast<long>(b);

    // If a isn't less or equal b, return false
    if (int_a > int_b) return false;
    if (int_a < int_b) return true;

    long dec_a = (a - int_a) * (std::pow(10, accuracy));
    long dec_b = (b - int_b) * (std::pow(10, accuracy));
    return dec_a <= dec_b;
  }

  bool ge(double a, double b, int accuracy = 4) {
    // Check integral parts first
    long int_a = static_cast<long>(a);
    long int_b = static_cast<long>(b);

    // If a isn't greater or equal b, return false
    if (int_a < int_b) return false;
    if (int_a > int_b) return true;

    long dec_a = (a - int_a) * (std::pow(10, accuracy));
    long dec_b = (b - int_b) * (std::pow(10, accuracy));
    return dec_a >= dec_b;
  }
}

#endif

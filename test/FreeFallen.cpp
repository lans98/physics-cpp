#include <FreeFallen.hpp>
#include <fstream>
#include <iostream>
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace fmt;
using namespace std;

int main() {
  double step, gravity, mass, constant;
  print("Size step, gravity, mass, constant and tolerance: ");
  cin >> step >> gravity >> mass >> constant;

  std::string analytic_file, numeric_file;
  print("Name for analytic data: ");
  cin.ignore(std::numeric_limits<streamsize>::max(), '\n');
  cin.clear();
  cin >> analytic_file;

  print("Name for numeric data: ");
  cin.ignore(std::numeric_limits<streamsize>::max(), '\n');
  cin.clear();
  cin >> numeric_file;

  ofstream analytic_data(analytic_file);
  ofstream numeric_data(numeric_file);

  auto exact_values = physics::real::free_fallen_analytic_for(gravity, mass, constant, step);

  print(analytic_data, "#Analytic data\n#Time Speed\n");
  for (auto &e : exact_values) 
    print(analytic_data, "{:.2f} {:.6f}\n", e.first, e.second);

  auto aprox_values = physics::real::free_fallen_numeric_for(gravity, mass, constant, step);

  print(numeric_data, "#Numeric data\n#Time Speed\n");
  for (auto &e : aprox_values)
    print(numeric_data, "{:.2f} {:.6f}\n", e.first, e.second);

  print("{:>14} & {:>14} & {:>14} & {:>14} \\\\ \\hline\n", "Time", "Numeric data", "Analytic data", "Error");
  auto min_size = std::min(exact_values.size(), aprox_values.size());
  auto eit = exact_values.begin();
  auto ait = aprox_values.begin();
  for (auto ii = 0; ii < min_size; ++ii, ++eit, ++ait)  {
    auto percentage_error = std::max(eit->second, ait->second) - std::min(eit->second, ait->second);
    percentage_error = std::max(eit->second, ait->second)? percentage_error / std::max(eit->second, ait->second) * 100 : 0;
    print("{:>14.2f} & {:>14.6f} & {:>14.6f} & {:>14.2f}\\% \\\\ \\hline\n", eit->first, eit->second, ait->second, percentage_error);
  }

  return 0;
}



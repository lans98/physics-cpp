#include <ParabolicMovement.hpp>
#include <Gopt.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

int main(int argc, char** argv) {
  double mass, initial_speed, initial_angle, gravity, step, c, ro, radio;
  std::string output_name_ideal;
  std::string output_name_real;

  gopt::GetOptParser getopt_parser(argc, argv, {
    {'i', "interactive", "Start in interactive mode (ignores all other flags!)", no_argument,
    [&]{
      std::memcpy(argv[optind + 1], "--", 2);
      fmt::print("Provide mass, initial speed, inital angle, gravity acceleration, step, c, ro, radio: ");
      std::cin >> mass >> initial_speed >> initial_angle >> gravity >> step >> c >> ro >> radio;
      fmt::print("File to save first case data: ");
      std::cin >> output_name_ideal;
      fmt::print("File to save second case data: ");
      std::cin >> output_name_real;
    }},
    {'m', "mass", "Set mass (double)", required_argument,
    [&]{
      mass = std::stod(optarg);
    }},
    {'s', "speed", "Set the initial speed (double)", required_argument,
    [&]{
      initial_speed = std::stod(optarg);
    }},
    {'a', "angle", "Set the initial angle (double)", required_argument,
    [&]{
      initial_angle = std::stod(optarg);
    }},
    {'g', "gravity", "Set the gravity constant (double)", required_argument,
    [&]{
      gravity = std::stod(optarg);
    }},
    {'t', "time-step", "Set the time step (double)", required_argument,
    [&]{
      step = std::stod(optarg);
    }},
    {'c', "constant", "Set the \'c\' constant (double)", required_argument,
    [&]{
      c = std::stod(optarg);
    }},
    {'p', "ro", "Set the \'ro\' constant (double)", required_argument,
    [&]{
      ro = std::stod(optarg);
    }},
    {'r', "radio", "Set the radio for this specific test case (double)", required_argument,
    [&]{
      radio = std::stod(optarg);
    }},
    {'1', "file1", "Set the name for the resulting data for case 1 (string)", required_argument,
    [&]{
      output_name_ideal = std::string(optarg);
    }},
    {'2', "file2", "Set the name for the resulting data for case 2 (string)", required_argument,
    [&]{
      output_name_real = std::string(optarg);
    }}
  });

  getopt_parser.parse([&]{
    if (optind == 1) {
      fmt::print("Provide mass, initial speed, inital angle, gravity acceleration, step, c, ro, radio: ");
      std::cin >> mass >> initial_speed >> initial_angle >> gravity >> step >> c >> ro >> radio;
      fmt::print("File to save first case data: ");
      std::cin >> output_name_ideal;
      fmt::print("File to save second case data: ");
      std::cin >> output_name_real;
    }
  });

  std::ofstream plotable_data1(output_name_ideal);
  if (!plotable_data1) return 1;

  std::ofstream plotable_data2(output_name_real);
  if (!plotable_data2) return 1;

  auto data1 = physics::ideal::parabolic_movement_ideal(mass, initial_speed, initial_angle, gravity, step);
  fmt::print("# Data for parabolic movement (ideal)\n#Time, x, y, speed_x, speed_y\n");
  fmt::print(plotable_data1, "# Plotable data for parabolic movement ideal\n# x y\n");

  for (auto &e : data1) {
    fmt::print("{:.3f}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", e.t, e.x, e.y, e.sx, e.sy);
    fmt::print(plotable_data1, "{:.6f} {:.6f}\n", e.x, e.y);
  }

  auto data2 = physics::real::parabolic_movement_real(mass, initial_speed, initial_angle, c, ro, physics::calc::circle_area(radio), gravity, step);
  fmt::print("# Data for parabolic movement (real)\n#Time, x, y, speed_x, speed_y\n");
  fmt::print(plotable_data1, "# Plotable data for parabolic movement real\n# x y\n");

  for (auto &e : data2) {
    fmt::print("{:.3f}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", e.t, e.x, e.y, e.sx, e.sy);
    fmt::print(plotable_data2, "{:.6f} {:.6f}\n", e.x, e.y);
  }
}
